import Vue from 'vue'
import App from './App.vue'
import store from './store'
import router from './router'
import Vant from 'vant';
import 'vant/lib/index.css';
import 'lib-flexible/flexible.js'
import {post,fetch} from './utils/request'
import axios from 'axios'
import 'vue2-toast/lib/toast.css';
import Toast from 'vue2-toast';
import animated from 'animate.css' // npm install animate.css --save安装，在引入
import { Lazyload } from 'vant';

// options 为可选参数，无则不传
Vue.use(Lazyload,{
    lazyComponent:true
});
Vue.use(animated)
Vue.use(Toast, {
    defaultType: 'center',
    duration: 3000
});
Vue.use(Vant);
Vue.config.productionTip = false
Vue.prototype.$axios = axios    //全局注册，使用方法为:this.$axios
Vue.prototype.$post=post;
Vue.prototype.$fetch=fetch;
router.beforeEach((to, from, next) => {
    // if (to.meta.title) {//判断是否有标题
    //     document.title = to.meta.title
    // }
    if (from.meta.allowBack == undefined) {
        next()
    }else if(from.meta.allowBack == false){

    }
})


new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
