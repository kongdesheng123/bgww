import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
      host:'https://h.xnal.cn',
      // alihost:'https://h.xnal.cn',
      channel:3,
      bargainCode:"",
      locationCode:"",
      activityId:1
  },
  mutations: {
      SET_BargainCode:(state,bargainCode)=>{
          state.bargainCode = bargainCode
      },
      SET_locationCode:(state,locationCode)=>{
          state.locationCode = locationCode
      },
      SET_AC:(state,activityId)=>{
          var ac = localStorage.setItem("activityId",activityId)
          state.activityId = ac
      }
  },
  actions: {

  },
  getters:{
      noLogin:(state)=>{
          window.location.href = state.host+"/login/auth?successUrl=/app/bgww/index&errorUrl=/app/bgww/error&unSubscribeUrl=/app/bgww/nofollow";
      },
      SET_HEI:()=>{
          return  window.innerHeight
      },
      currentTime:state => (date) =>{
          return date.substring(0,10);
      },
      getCookie:state => (objName) =>{
          var arrStr = document.cookie.split("; ");
          for (var i = 0; i < arrStr.length; i++) {
              var temp = arrStr[i].split("=");
              if (temp[0] == objName){
                  return decodeURI(temp[1]);
              }
          }
      },
      change_tab:(state)=>(tab,active)=>{
        if(tab){
            switch(active) {
                case 0:
                    router.replace({
                        path: `/index`
                    })
                    break;
                case 1:
                    router.replace({
                        path: `/coupon`
                    })
                    break;
                case 2:
                    router.replace({
                        path: `/bag`
                    })
                    break;
                case 3:
                    router.replace({
                        path: `/mine`
                    })
                    break;
                default:
            }
        }else{
            switch(active) {
                case 0:
                    router.replace({
                        path: `/index`
                    })
                    break;
                case 1:
                    router.replace({
                        path: `/bag`
                    })
                    break;
                case 2:
                    router.replace({
                        path: `/mine`
                    })
                    break;
                default:
            }
        }
      },
      show_tab:(state, getters)=>{
          var activityId = getters.getCookie("activityId") || "";
          return activityId !=="" ? true : false;
      },
      handleUrl:(state, getters) => (url) =>{
          if (url) {
              let missionId = getters.getQueryString(url, "missionId");
              if (missionId) {//日常活动
                  localStorage.setItem("missionId",missionId);
              }
              let fromV = getters.getQueryString(url, "from");
              if (fromV) {//特邀嘉宾
                  localStorage.setItem("fromV",fromV);
              }
              let locationCode = getters.getQueryString(url, "lc");
              if (locationCode) {//来源点位
                  console.log("保存点位+++++++++"+locationCode)
                  localStorage.setItem("locationCode",locationCode);
                  // this.state.locationCode = locationCode;//20240;//20016;//
                  // console.log(this.state.locationCode)
              }
              let bargainCode = getters.getQueryString(url, "bc");
              if (bargainCode) {
                  localStorage.setItem("bargainCode",bargainCode);
                  // state.bargainCode = bargainCode;
              }
              let card = getters.getQueryString(url, "card");
              if (card) {//来源点位
                  localStorage.setItem("card",card);
              }
          }
      },
      handleTopupItems:state => (items) =>{
          items.forEach(function (value, index, array) {
              var gift = JSON.parse(value.gift);
              var giftstr = '';
              if (gift.coin > 0) {
                  giftstr = giftstr + "送" + gift.coin + "币";
                  let couponList = value.couponList;
                  var couponStr = '';
                  if (couponList.length > 0) {
                      for (var i = 0; i < couponList.length; i++) {
                          couponStr = couponStr + "+" + couponList[i].name;
                      }
                      couponStr = couponStr.substring(1);
                      value['couponStr'] = couponStr;
                      giftstr = giftstr + "+" + couponStr;
                  }
                  giftstr = giftstr;
              } else {
                  giftstr = '';
              }
              value['gifts'] = giftstr;
              value['gift'] = gift;
          })
          return items
      },
      getQueryString:state => (url,name) =>{
          var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
          var index = url.indexOf("?") + 1;
          var r = url.substr(index).match(reg);
          if (r != null) return unescape(r[2]); return null;
      }
  }
})
