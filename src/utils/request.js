import axios from 'axios'
import store from '../store'
import qs from 'qs'
axios.defaults.timeout = 15000;
// if (/MicroMessenger/.test(window.navigator.userAgent)) {
//     axios.defaults.baseURL = store.state.Wxhost;
// }else if(/AlipayClient/.test(window.navigator.userAgent)){
//     axios.defaults.baseURL = store.state.alihost;
// }
axios.defaults.baseURL = store.state.host;
//http request 拦截器
axios.interceptors.request.use(
  config => {
    // const token = getCookie('名称');注意使用的时候需要引入cookie方法，推荐js-cookie
    // config.data = JSON.stringify(config.data);
    config.headers = {
       // 'Content-Type':'application/x-www-form-urlencoded'
        'Content-Type' : 'application/x-www-form-urlencoded;charset=UTF-8'
    }
    // if(token){
    //   config.params = {'token':token}
    // }
    return config;
  },
  error => {
    return Promise.reject(err);
  }
);

//
//http response 拦截器//这个暂未使用
axios.interceptors.response.use(
  response => {
    if(response.data.errCode == 2){

    }
    return response;
  },
  error => {
    return Promise.reject(error)
  }
)


/**
 * 封装get方法
 * @param url
 * @param data
 * @returns {Promise}
 */

export function fetch(url,params={}){
  return new Promise((resolve,reject) => {
    axios.get(url,{
      params:params
    })
      .then(response => {
        resolve(response.data);
      })
      .catch(err => {
        reject(err)
      })
  })
}


/**
 * 封装post请求
 * @param url
 * @param data
 * @returns {Promise}
 */

export function post(url,data = {}){
  return new Promise((resolve,reject) => {
    axios.post(url,qs.stringify(data))
      .then(response => {
        resolve(response.data);
      },err => {
        reject(err)
      })
  })
}

/**
 * 封装patch请求
 * @param url
 * @param data
 * @returns {Promise}
 */

export function patch(url,data = {}){
  return new Promise((resolve,reject) => {
    axios.patch(url,data)
      .then(response => {
        resolve(response.data);
      },err => {
        reject(err)
      })
  })
}

/**
 * 封装put请求
 * @param url
 * @param data
 * @returns {Promise}
 */

export function put(url,data = {}){
  return new Promise((resolve,reject) => {
    axios.put(url,data)
      .then(response => {
        resolve(response.data);
      },err => {
        reject(err)
      })
  })
}
