import Vue from 'vue'
import Router from 'vue-router'
import records from './views/records/records.vue'
import error from './views/error/error.vue'
import detail from './views/community/details/details.vue'
import transfer  from './views/transfer/transfer'
import webview  from './views/webview/webview'
import video  from './views/video/video'
Vue.use(Router)

export default new Router({
    mode: 'history',
    base: '/app/bgww',
    routes: [
        {
            path: '/video',
            name: 'video',
            component: video,
        },
        {
            path: '/webview',
            name: 'webview',
            component: webview,
        },
        {
            path: '/webview',
            name: 'webview',
            component: webview,
        },
        {
            path: '/transfer',
            name: 'transfer',
            component: transfer,
        },
        {
            path: '/eggtransfer',
            name: 'eggtransfer',
            component: () => import('./views/transfer/eggtransfer')
        },
        {
            path: '/index',
            name: 'index',
            component: () => import('./views/index/index')
        },
        {
            path: '',
            name: 'index',
            component: () => import('./views/index/index')
        },
        {
            path: '/',
            name: 'index',
            component: () => import('./views/index/index')
        },
        {
            path: '/buy',
            name: 'buy',
            component: () => import('./views/buy/buy')
        },
        {
            path: '/nogoods',
            name: 'nogoods',
            component: () => import('./views/no_goods/no_goods')
        },
        {
            path: '/error',
            name: 'error',
            component: error,
        },
        {
            path: '/details',
            name: 'details',
            component: detail,
            meta: {
                title:"详情"
            }
        },
        {
            path: '/community',
            name: 'community',
            component: () => import('./views/community/community'),
            meta: {
                title:"互动"
            }
        },
        {
            path: '/records',
            name: 'records',
            component: records
        },
        {
            path: '/Jigsaw',
            name: 'Jigsaw',
            component: () => import('./views/Jigsaw/Jigsaw')
        },
        {
            path: '/bag',
            name: 'bag',
            component: () => import('./views/bag/bag'),
            meta: {
                title:''
            }
        },
        {
            path: '/friend_bag',
            name: 'friend_bag',
            component: () => import('./views/friend_bag/friend_bag')
        },
        {
            path: '/admin_address',
            name: 'admin_address',
            component: () => import('./views/admin_address/admin_address')
        },
        {
            path: '/once_address',
            name: 'once_address',
            component: () => import('./views/add_address/once_address')
        },
        {
            path: '/has_address',
            name: 'has_address',
            component: () => import('./views/admin_address/has_address')
        },
        {
            path: '/add_address',
            name: 'add_address',
            component: () => import('./views/add_address/add_address')
        },
        {
            path: '/scratch_card',
            name: 'scratch_card',
            // component: scratch_card
            component: () => import('./views/scratch_card/scratch_card')
        },
        {
            path: '/win',
            name: 'win',
            component: () => import('./views/win/win')
        },
        {
            path: '/syn_result',
            name: 'syn_result',
            component: () => import('./views/win/syn_result')
        },
        {
            path: '/used',
            name: 'used',
            component: () => import('./views/win/used')
        },
        {
            path: '/eggused',
            name: 'used',
            component: () => import('./views/prize/used/used')
        },
        {
            path: '/busy',
            name: 'busy',
            component: () => import('./views/win/busy')
        },
        {
            path: '/logistics',
            name: 'logistics',
            component: () => import('./views/logistics/logistics')
        },
        {
            path: '/nofollow',
            name: 'nofollow',
            component: () => import('./views/win/nofollow')
        },
        {
            path: '/qrcode',
            name: 'qrcode',
            component: () => import('./views/qrcode/qrcode')
        },
        {
            path: '/dcard',
            name: 'dcard',
            component: () => import('./views/dcard/dcard')
        },
        {
            path: '/topup',
            name: 'topup',
            component: () => import('./views/topup/topup')
        },
        {
            path: '/mine',
            name: 'mine',
            component: () => import('./views/mine/mine')
        },
        {
            path: '/mine/coin',
            name: 'coin',
            component: () => import('./views/mine/coin/coin')
        },
        {
            path: '/mine/coupon',
            name: 'coupon',
            component: () => import('./views/mine/coupon/coupon')
        },
        {
            path: '/pay_success',
            name: 'pay_success',
            component: () => import('./views/pay_success/pay_success')
        },
        {
            path: '/buytime',
            name: 'buytime',
            component: () => import('./views/buy/buytime')
        },
        {
            path: '/discount',
            name: 'discount',
            component: () => import('./views/discount/discount')
        },
        {
            path: '/onlywx',
            name: 'onlywx',
            component: () => import('./views/discount/onlywx')
        },
        {
            path: '/coupon',
            name: 'coupon',
            component: () => import('./views/prize/coupon')
        },
        {
            path: '/activity',
            name: 'activity',
            component: () => import('./views/prize/activity/index')
        },
        {
            path: '/cash',
            name: 'cash',
            component: () => import('./views/prize/cash/cash')
        },
        {
            path: '/receive',
            name: 'receive',
            component: () => import('./views/prize/receive/index')
        },
        {
            path: '/eggJigsaw',
            name: 'Jigsaw',
            component: () => import('./views/prize/Jigsaw/Jigsaw')
        },
        {
            path: '/noactivity',
            name: 'noactivity',
            component: () => import('./views/prize/used/noactivity')
        },
        {
            path: '/over',
            name: 'over',
            component: () => import('./views/prize/used/over')
        },

        // {
        //     path: '/couponlist',
        //     name: 'couponlist',
        //     component: () => import('./views/activity/couponlist/couponlist'),
        //     meta: {
        //         title:''
        //     }
        // },
        // {
        //     path: '/play',
        //     name: 'play',
        //     component: () => import('./views/activity/play/play'),
        //     meta: {
        //         title:''
        //     }
        // },
        // {
        //     path: '/search',
        //     name: 'search',
        //     component: () => import('./views/activity/search/search'),
        //     meta: {
        //         title:''
        //     }
        // },
        // {
        //     path: '/shoplist',
        //     name: 'shoplist',
        //     component: () => import('./views/activity/shoplist/shoplist'),
        //     meta: {
        //         title:''
        //     }
        // },
        // {
        //     path: '/game',
        //     name: 'game',
        //     component: () => import('./views/activity/game/index'),
        // },
    ]
})
