module.exports = {
  publicPath: '/app/bgww',
  lintOnSave:false,
  assetsDir: undefined,
  runtimeCompiler: undefined,
  productionSourceMap: undefined,
  parallel: undefined,
  devServer: {
      open:false,//启动项目后自动开启浏览器
      port: 9527,     // 端口
      hot:true,
      host: '0.0.0.0',
      disableHostCheck: true,
      // proxy: {
      //     '/api': {
      //         // 目标 API 地址
      //         target: 'http://192.168.0.77:8080/',
      //         // 如果要代理 websockets
      //         ws: true,
      //         // 将主机标头的原点更改为目标URL
      //         changeOrigin: false
      //     }
      // }
  },
  css: {
    loaderOptions: {
        postcss: {
            plugins: [
                require('postcss-plugin-px2rem')({ //配置项，详见官方文档
                    rootValue: 75,
                    exclude: /(node_module)/,
                }), // 换算的基数
            ]
        }
    }
  },
  outputDir: 'vue',
}
